from rest_framework import routers

from server.views import ServerViewSet

router = routers.DefaultRouter()
router.register('servers', ServerViewSet)

urlpatterns = router.urls
