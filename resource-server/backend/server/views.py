import logging

from backend.views import ModelViewSet
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import APIException
from rest_framework.response import Response

from server.models import Server
from server.serializers import ServerSerializer

logger = logging.getLogger()


# Create your views here.
class ServerViewSet(ModelViewSet):
    queryset = Server.objects.all()
    serializer_class = ServerSerializer

    @extend_schema(request=ServerSerializer(many=True))
    @action(detail=False, methods=['post'], url_path='batch-create')
    def batch_create(self, request, *args, **kwargs):
        serializer = ServerSerializer(data=request.data, many=True, context={"request": request})
        serializer.is_valid(raise_exception=True)
        errors = self.perform_batch_create(serializer)
        if errors:
            raise APIException('Batch create failed at items: [{}]').format(', '.join(errors))

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_batch_create(self, serializer):
        instances = []
        errors = []
        for idx, item in enumerate(serializer.validated_data):
            child_serializer = serializer.child.__class__()
            child_serializer._validated_data = item
            child_serializer._errors = {}
            try:
                self.perform_create(child_serializer)
            except Exception as e:
                logger.error(
                    f'Error while performing batch create for {serializer.validated_data}, '
                    f'item at index {idx} got an error: {e}', exc_info=True
                )
                errors.append(str(idx))
            finally:
                # If an exception occurs, child_serializer.instance = None
                instances.append(child_serializer.instance)

        serializer.instance = instances
        return errors
