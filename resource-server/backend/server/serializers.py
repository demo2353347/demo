from backend.serializers import BaseModelSerializer, ResourceSerializerMixin

from server.models import Server


class ServerSerializer(BaseModelSerializer):
    class Meta:
        model = Server
        fields = ['id', 'name', 'os', 'ram', 'cpu', 'ip_address', 'disk_type', 'disk_size']
