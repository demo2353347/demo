from django.contrib import admin

from server.models import Server


# Register your models here.
@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'os', 'ram', 'cpu', 'ip_address', 'disk_type', 'disk_size')
