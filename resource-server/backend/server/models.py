from django.db import models

from server.choices import SERVER_DISK_TYPE_CHOICES, SERVER_DISK_TYPE_SSD
from backend.models import BaseModel

# Create your models here.
class Server(BaseModel):
    name = models.CharField(
        max_length=255
    )
    os = models.CharField(
        max_length=100,
        default='',
    )
    ram = models.PositiveIntegerField(
        default=0
    )
    cpu = models.PositiveIntegerField(
        default=0
    )
    ip_address = models.GenericIPAddressField(
        blank=True,
        null=True,
    )
    disk_type = models.CharField(
        max_length=20,
        choices=SERVER_DISK_TYPE_CHOICES,
        default=SERVER_DISK_TYPE_SSD
    )
    disk_size = models.IntegerField(
        null=True,
        blank=True,
    )
