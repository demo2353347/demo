from backend.models import BaseModel
from django.db import models

from certificate.models import Certificate


class LoadBalancer(BaseModel):
    name = models.CharField(
        max_length=100
    )
    public_ip = models.GenericIPAddressField(null=True)
    public_port = models.PositiveIntegerField()
    certificate = models.ForeignKey(
        null=True,
        to=Certificate,
        on_delete=models.PROTECT,
        related_name='load_balancers'
    )
    description = models.CharField(null=True, blank=True, max_length=255)

    def save(self, **kwargs):
        self.public_ip = '10.0.0.0'
        return super(LoadBalancer, self).save(**kwargs)


class LBServer(BaseModel):
    load_balancer = models.ForeignKey(
        to=LoadBalancer,
        on_delete=models.CASCADE,
        related_name='members'
    )
    name = models.CharField(
        max_length=100
    )
    address = models.GenericIPAddressField(
        null=True
    )
    port = models.PositiveIntegerField()
