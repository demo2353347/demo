from backend.views import ModelViewSet

from load_balancer.models import LoadBalancer, LBServer
from load_balancer.serializers import LoadBalancerSerializer, LBServerSerializer


# Create your views here.
class LoadBalancerViewSet(ModelViewSet):
    queryset = LoadBalancer.objects.all()
    serializer_class = LoadBalancerSerializer


class LBServerViewSet(ModelViewSet):
    queryset = LBServer.objects.all()
    serializer_class = LBServerSerializer
