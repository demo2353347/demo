from backend.serializers import BaseModelSerializer
from rest_framework import serializers

from certificate.models import Certificate
from load_balancer.models import LBServer, LoadBalancer


class LoadBalancerSerializer(BaseModelSerializer):
    certificate = serializers.SlugRelatedField(allow_null=True, queryset=Certificate.objects.all(), slug_field='name')

    class Meta:
        model = LoadBalancer
        fields = ['id', 'name', 'public_ip', 'public_port', 'certificate', 'description']


class LBServerSerializer(BaseModelSerializer):
    class Meta:
        model = LBServer
        fields = ['id', 'name', 'load_balancer', 'address', 'port']
