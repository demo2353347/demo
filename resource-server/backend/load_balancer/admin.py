from django.contrib import admin

from load_balancer.models import LoadBalancer, LBServer


# Register your models here.
@admin.register(LoadBalancer)
class LoadBalancerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'public_ip', 'public_port', 'certificate', 'description')


@admin.register(LBServer)
class LBServerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'load_balancer', 'address', 'port')
