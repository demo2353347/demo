from rest_framework import routers

from load_balancer.views import LoadBalancerViewSet, LBServerViewSet

router = routers.DefaultRouter()
router.register('load-balancers', LoadBalancerViewSet)
router.register('lb-servers', LBServerViewSet)

urlpatterns = router.urls
