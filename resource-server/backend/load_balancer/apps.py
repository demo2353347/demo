from django.apps import AppConfig


class LoadBalancerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'load_balancer'
    kc_config = {
        'SERVER_URL': 'http://127.0.0.1:8080/',
        'REALM_NAME': 'demo',
        'PUBLIC_KEY': 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtRMdbPtKhixkmjxloCBsgg1gFPFBUZXOoDvUwq2d+K8HrUZAyrWWsQGgFuCxGFsyT2pw5KeCcKNr1OD4AOoALUx/ahhwItEBGQRKwcE94ZxZrfg0rBhueTk6pbiFMEQi8Cz57B6975mLH4ic3MDg1WOhznwn4y5GP1cPfz1p+oIYiLJmFgnlv+PA9CCZWgsbHNRNMNR+TleebeI3BZDb0LtmsztZZnfcdEA27pWVYPWpgskAj9TLu8ieKPqHNMXnBkaOVZUw4+WNGZrZHh9Avl+4A1IEvWFV4vQ2QWE6qsLoqTjWLgxlEhUFDORbcdzr8NBl03Q8eM2i8cuX4tQk9QIDAQAB',
        'CLIENT_ID': 'load-balancer',
        'CLIENT_SECRET_KEY': 'leOh8YpDHgZB1tDPL020OLV0saD9jlsw',
        'VERIFY_SSL': False,
    }