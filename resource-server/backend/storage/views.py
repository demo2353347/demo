from backend.views import ModelViewSet

from storage.models import Volume
from storage.serializers import VolumeSerializer


# Create your views here.
class VolumeViewSet(ModelViewSet):
    queryset = Volume.objects.all()
    serializer_class = VolumeSerializer
