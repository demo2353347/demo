from backend.models import BaseModel
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from storage.choices import VOLUME_TYPE_CHOICES


# Create your models here.
class Volume(BaseModel):
    name = models.CharField(
        max_length=255
    )
    size = models.DecimalField(
        decimal_places=0,
        max_digits=4,
        validators=[MinValueValidator(1), MaxValueValidator(2048)],
        verbose_name='Size (GB)'
    )
    type = models.CharField(
        max_length=20,
        choices=VOLUME_TYPE_CHOICES,
    )

    def __str__(self):
        return self.name
