from django.contrib import admin

from storage.models import Volume


# Register your models here.
@admin.register(Volume)
class VolumeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'size', 'type')
