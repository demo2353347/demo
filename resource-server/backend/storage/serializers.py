from backend.serializers import BaseModelSerializer

from storage.models import Volume


class VolumeSerializer(BaseModelSerializer):
    class Meta:
        model = Volume
        fields = ['id', 'name', 'size', 'type']
