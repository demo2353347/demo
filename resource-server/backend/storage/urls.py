from rest_framework import routers

from storage.views import VolumeViewSet

router = routers.DefaultRouter()
router.register('volumes', VolumeViewSet)

urlpatterns = router.urls
