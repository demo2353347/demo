# Generated by Django 4.2.6 on 2023-12-03 09:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0002_remove_volume_project'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='volume',
            name='owner',
        ),
    ]
