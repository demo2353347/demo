# Generated by Django 4.2.6 on 2023-11-26 14:15

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('tenant', '0002_project'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Volume',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('size', models.DecimalField(decimal_places=0, max_digits=4, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(2048)], verbose_name='Size (GB)')),
                ('type', models.CharField(choices=[('nvme', 'NVMe'), ('ssd', 'SSD'), ('sas', 'SAS'), ('hdd', 'HDD')], max_length=20)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='tenant.project')),
            ],
            options={
                'ordering': ('-updated_at', 'pk'),
                'abstract': False,
            },
        ),
    ]
