from rest_framework import routers

from certificate.views import CertificateViewSet

router = routers.DefaultRouter()
router.register('certs', CertificateViewSet)

urlpatterns = router.urls
