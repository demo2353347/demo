from backend.views import ModelViewSet

from certificate.models import Certificate
from certificate.serializers import CertificateSerializer


# Create your views here.
class CertificateViewSet(ModelViewSet):
    queryset = Certificate.objects.all()
    serializer_class = CertificateSerializer