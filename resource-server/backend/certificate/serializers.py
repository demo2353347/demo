from backend.serializers import BaseModelSerializer, ResourceSerializerMixin

from certificate.models import Certificate


class CertificateSerializer(BaseModelSerializer):
    class Meta:
        model = Certificate
        fields = ['id', 'name', 'public_key', 'fingerprint', 'not_valid_before', 'not_valid_after']
