from django.db import models

from backend.models import BaseModel
from tenant.models import User


# Create your models here.
class Certificate(BaseModel):
    name = models.CharField(max_length=255, unique=True)
    public_key = models.TextField(null=True)
    fingerprint = models.CharField(max_length=255)
    not_valid_before = models.DateTimeField(null=True)
    not_valid_after = models.DateTimeField(null=True)

    def __str__(self):
        return self.name
