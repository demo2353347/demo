
class InvalidExtraClaim(Exception):
    default_message = 'The value of each claim must be array of string'

    def __init__(self, msg=None, *args, **kwargs):
        msg = msg or self.default_message
        super(InvalidExtraClaim, self).__init__(msg, *args, **kwargs)
