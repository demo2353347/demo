from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field
from drf_writable_nested import NestedCreateMixin, NestedUpdateMixin
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


class BaseModelSerializer(ModelSerializer):
    display_name = serializers.SerializerMethodField(read_only=True)

    @extend_schema_field(OpenApiTypes.STR)
    def get_display_name(self, obj):
        return str(obj)


class WritableNestedModelSerializer(NestedCreateMixin, NestedUpdateMixin,
                                    BaseModelSerializer):
    pass


class ResourceSerializerMixin:
    """
    Extract owner from request and update to serializer data
    """

    def to_internal_value(self, data):
        data = super().to_internal_value(data)
        request = self.context.get('request')
        data['owner'] = request.user
        return data
