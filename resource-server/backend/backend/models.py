import uuid

from backend.utils import auto_rollback
from django.db import models


class BaseModel(models.Model):
    """
    BaseModel is the base model for all objects that are identified via uuid
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.__class__.__name__}({self.id})'

    class Meta:
        ordering = ('-updated_at', 'pk')
        abstract = True

    def validate_delete(self):
        """Check if object can be deleted.

        If object can be deleted via the delete method, rollback the transaction
        to perform other actions before actually deleting it.

        Otherwise, a ProtectedError is raised and will be captured by the
        default exception handler of backend.ViewSet.

        This might cause a performance hit if there are a lot of deleting
        operations - a topic for future improvements.
        """
        assert self.pk, 'Instance must present in database in order to perform delete validation'
        with auto_rollback():
            self.__class__.objects.get(pk=self.pk).delete()


class ResourceModel(BaseModel):
    """
    ResourceModel is the base model for all resource models
    """
    owner = models.ForeignKey(to='tenant.User', on_delete=models.PROTECT)

    class Meta(BaseModel.Meta):
        abstract = True
