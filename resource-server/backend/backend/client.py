from jose import JWTError
from keycloak import KeycloakOpenID, KeycloakOpenIDConnection, KeycloakUMA
from keycloak.exceptions import raise_error_from_response, KeycloakPostError
from keycloak.urls_patterns import URL_TOKEN
from rest_framework.exceptions import AuthenticationFailed

from backend import settings


class KeycloakClient:
    def __init__(self):
        self.kc_config = getattr(settings, 'KEYCLOAK_CONFIG')
        self.kc_public_key = (
            f'-----BEGIN PUBLIC KEY-----\n'
            f"{self.kc_config['PUBLIC_KEY']}\n"
            f'-----END PUBLIC KEY-----'
        )
        self._client = None
        self._kc_uma = None

    @property
    def client(self):
        if not self._client:
            self._client = KeycloakOpenID(
                server_url=self.kc_config['SERVER_URL'],
                realm_name=self.kc_config['REALM_NAME'],
                client_id=self.kc_config['CLIENT_ID'],
                client_secret_key=self.kc_config['CLIENT_SECRET_KEY'],
                verify=self.kc_config['VERIFY_SSL'])
        return self._client

    @property
    def kc_uma(self):
        if not self._kc_uma:
            keycloak_connection = KeycloakOpenIDConnection(
                server_url=self.kc_config['SERVER_URL'],
                realm_name=self.kc_config['REALM_NAME'],
                client_id=self.kc_config['CLIENT_ID'],
                client_secret_key=self.kc_config['CLIENT_SECRET_KEY']
            )
            self._kc_uma = KeycloakUMA(connection=keycloak_connection)
        return self._kc_uma

    def load_token(self, token):
        """
        Decode and return token decoded information
        """
        try:
            return self.client.decode_token(token, self.kc_public_key)
        except JWTError as e:
            raise AuthenticationFailed('Invalid access token: {}'.format(e))

    def obtain_rpt(
            self,
            access_token="",
            ticket="",
            grant_type="urn:ietf:params:oauth:grant-type:uma-ticket",
            audience="backend",
            permission=None
    ):
        params_path = {"realm-name": self.client.realm_name}
        payload = {
            "grant_type": grant_type,
            "audience": audience,
        }
        if permission:
            payload["permission"] = permission
        if ticket:
            payload["ticket"] = ticket
            payload["submit_request"] = True
        self.client.connection.clean_headers()
        self.client.connection.add_param_headers("Authorization", "Bearer " + access_token)
        data_raw = self.client.connection.raw_post(URL_TOKEN.format(**params_path), data=payload)
        return raise_error_from_response(data_raw, KeycloakPostError)
