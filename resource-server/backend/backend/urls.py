"""
URL configuration for backend project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import environ
from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularSwaggerView, SpectacularAPIView

env = environ.Env()

service_mapping = {
    'k8s': path('k8s/', include('k8s.urls')),
    'load_balancer': path('load_balancer/', include('load_balancer.urls')),
    'storage': path('storage/', include('storage.urls')),
    'server': path('server/', include('server.urls')),
    'certificate': path('certificate/', include('certificate.urls')),
    'action_log': path('action_log/', include('action_log.urls'))
}

service = env('SERVICE')

urlpatterns = [path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
               path('', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
               path('admin/', admin.site.urls),
               service_mapping[service]
               ]
#
# urlpatterns = [
#     path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
#     path('', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
#     path('admin/', admin.site.urls),
#     path('k8s/', include('k8s.urls')),
#     path('load_balancer/', include('load_balancer.urls')),
#     path('storage/', include('storage.urls')),
#     path('server/', include('server.urls')),
#     path('certificate/', include('certificate.urls')),
#     path('action_log/', include('action_log.urls'))
# ]
