import logging

from backend.permissions.permissions import IsAuthenticated, HasUserPermission
from rest_framework.mixins import (ListModelMixin, RetrieveModelMixin, CreateModelMixin, UpdateModelMixin,
                                   DestroyModelMixin)
from rest_framework.viewsets import GenericViewSet

from action_log.models import ActionLog

logger = logging.getLogger(__name__)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class ActionLogMixin(object):
    def finalize_response(self, request, response, *args, **kwargs):
        response = super(ActionLogMixin, self).finalize_response(request, response, *args, **kwargs)
        try:
            ActionLog.objects.create(path=request.path,
                                     method=request.method,
                                     username=request.user.username,
                                     remote_ip=get_client_ip(request),
                                     status_code=response.status_code)
        except Exception:
            pass

        return response


# Create your views here.
class ModelViewSet(ActionLogMixin, ListModelMixin, RetrieveModelMixin, CreateModelMixin, UpdateModelMixin,
                   DestroyModelMixin,
                   GenericViewSet):
    permission_classes = [IsAuthenticated, HasUserPermission]


class ReadOnlyModelViewSet(ActionLogMixin, ListModelMixin, RetrieveModelMixin, GenericViewSet):
    pass
