from backend.client import KeycloakClient
from django.contrib.auth import get_user_model
from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed


class KeycloakAuthentication(TokenAuthentication):
    keyword = 'Bearer'

    def __init__(self):
        self.kc_client = KeycloakClient()
        self.UserModel = get_user_model()

    def authenticate_credentials(self, access_token):
        token_info = self.kc_client.load_token(access_token)
        try:
            user_info = {
                'pk': token_info['sub'],
                'username': token_info['preferred_username'],
                'password': None,
                'email': token_info.get('email', None),
                'first_name': token_info.get('given_name', ''),
                'last_name': token_info.get('family_name', '')
            }
        except KeyError:
            raise AuthenticationFailed(f'JWT access token is missing some keys')
        user, created = self.UserModel.objects.get_or_create_user(user_info)
        if not created and not user.is_active:
            raise exceptions.AuthenticationFailed('User inactive or deleted.')

        return user, None
