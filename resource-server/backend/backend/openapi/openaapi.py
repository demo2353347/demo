from django.utils.translation import gettext_lazy as _
from drf_spectacular.utils import OpenApiResponse, PolymorphicProxySerializer
from rest_framework import serializers


class NonFieldErrorResponseSerializer(serializers.Serializer):
    detail = serializers.CharField()


class FieldErrorResponseSerializer(serializers.Serializer):
    detail = serializers.JSONField()


error_response = PolymorphicProxySerializer(
    component_name='ErrorResponse',
    serializers=[NonFieldErrorResponseSerializer, FieldErrorResponseSerializer],
    resource_type_field_name='detail'
)


class ConflictNonFieldErrorResponseSerializer(NonFieldErrorResponseSerializer):
    dependant_objects = serializers.ListField(child=serializers.JSONField(), allow_null=True, required=False)


conflict_error_response = PolymorphicProxySerializer(
    component_name='ConflictErrorResponse',
    serializers=[ConflictNonFieldErrorResponseSerializer, FieldErrorResponseSerializer],
    resource_type_field_name='detail'
)

OpenApiResponse = OpenApiResponse
RESPONSE_200 = OpenApiResponse(description=_('Request was successful.'))
RESPONSE_201 = OpenApiResponse(description=_('The request succeeded, and a new resource was created as a result.'))
RESPONSE_202 = OpenApiResponse(description=_('Request is accepted, but processing may take some time.'))
RESPONSE_203 = OpenApiResponse(description=_('Returned information is not full set, but a subset.'))
RESPONSE_204 = OpenApiResponse(description=_('Request fulfilled but nothing returned.'))
RESPONSE_400 = OpenApiResponse(response=error_response,
                               description=_('Some content in the request was invalid.'))
RESPONSE_401 = OpenApiResponse(response=error_response,
                               description=_('Access is denied due to invalid credentials.'))
RESPONSE_403 = OpenApiResponse(response=error_response,
                               description=_('Policy does not allow current user to do this operation.'))
RESPONSE_404 = OpenApiResponse(response=error_response,
                               description=_('The requested resource could not be found.'))
RESPONSE_409 = OpenApiResponse(response=conflict_error_response,
                               description=_('Request conflicts with the current state of the server.'))
RESPONSE_412 = OpenApiResponse(response=error_response,
                               description=_('Access to the target resource has been denied.'))
RESPONSE_500 = OpenApiResponse(description=_('Something went wrong.'))
RESPONSE_503 = OpenApiResponse(description=_('The service cannot handle the request right now.'))
COMMON_RESPONSES = {
    '200': RESPONSE_200,
    '201': RESPONSE_201,
    '202': RESPONSE_202,
    '203': RESPONSE_203,
    '204': RESPONSE_204,
    '400': RESPONSE_400,
    '401': RESPONSE_401,
    '403': RESPONSE_403,
    '404': RESPONSE_404,
    '409': RESPONSE_409,
    '412': RESPONSE_412,
    '500': RESPONSE_500,
    '503': RESPONSE_503,
}


def build_example_response(custom_responses=None, custom_201_resp=None):
    example_response = {
        200: RESPONSE_200,
        201: RESPONSE_201,
        202: RESPONSE_202,
        204: RESPONSE_204,
        400: RESPONSE_400,
        403: RESPONSE_403,
        404: RESPONSE_404,
        409: RESPONSE_409,
        500: RESPONSE_500,
    }
    if custom_responses:
        example_response.update(custom_responses)
    return example_response
