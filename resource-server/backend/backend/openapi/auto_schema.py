import re

from backend.openapi.openaapi import COMMON_RESPONSES
from drf_spectacular import openapi
from drf_spectacular.plumbing import append_meta, build_basic_type, force_instance
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiResponse
from netfields.rest_framework import (
    InetAddressField, CidrAddressField, MACAddressField, MACAddress8Field
)
from rest_framework.viewsets import GenericViewSet, ViewSet


class AutoSchema(openapi.AutoSchema):
    def get_operation_id(self):
        tokenized_path = self._tokenize_path()
        try:
            if isinstance(self.view, (ViewSet, GenericViewSet)):
                # TODO: WTF???
                view = self.view
                extra_actions = self.view.__class__.get_extra_actions()
                self.view = view
            else:
                extra_actions = []
        except AttributeError:
            extra_actions = []

        view_action = getattr(self.view, 'action', None)
        for func in extra_actions:
            if func.url_path == tokenized_path[-1] and view_action:
                tokenized_path[-1] = view_action

        # replace dashes as they can be problematic later in code generation
        tokenized_path = [t.replace('-', '_') for t in tokenized_path]

        if self.method == 'GET' and self._is_list_view():
            action = 'list'
        else:
            action = self.method_mapping[self.method.lower()]

        if action == 'create' and view_action != action:
            action = []
        else:
            action = [action]

        if not tokenized_path:
            tokenized_path.append('root')

        if re.search(r'<drf_format_suffix\w*:\w+>', self.path_regex):
            tokenized_path.append('formatted')

        return '_'.join(tokenized_path + action)

    def get_filter_backends(self):
        """Remove filters for batch operations"""
        try:
            if self.view.action.startswith('batch_'):
                return []
        except AttributeError:
            pass

        return super(AutoSchema, self).get_filter_backends()

    def _map_serializer_field(self, field, direction, bypass_extensions=False):
        """Specifies base type for custom fields"""
        meta = self._get_serializer_field_meta(field, direction)

        # Resolve types for netfields serializers
        if (isinstance(field, InetAddressField)
                or isinstance(field, CidrAddressField)
                or isinstance(field, MACAddressField)
                or isinstance(field, MACAddress8Field)):
            return append_meta(build_basic_type(OpenApiTypes.STR), meta)

        return super(AutoSchema, self)._map_serializer_field(field, direction, bypass_extensions=False)

    def _get_response_bodies(self):
        """Appends common error codes for response body"""
        responses = super(AutoSchema, self)._get_response_bodies()
        if self.method == 'GET':
            if self._is_list_view():
                error_codes = ['400', '401', '403', '500', '503']
            else:
                error_codes = ['400', '401', '403', '404', '500', '503']
        else:
            error_codes = ['400', '401', '403', '404', '409', '500', '503']

        for code in error_codes:
            if code not in responses:
                resp = COMMON_RESPONSES[code]
                responses[code] = self._get_response_for_code(resp, code)

        return responses

    def _is_list_view(self, serializer=None):
        """Extracts serializer from OpenApiResponse input"""
        if serializer is None:
            serializer = self.get_response_serializers()

        if isinstance(serializer, dict) and serializer:
            # extract likely main serializer from @extend_schema override
            serializer = {str(code): s for code, s in serializer.items()}
            serializer = serializer[min(serializer)]

        if isinstance(serializer, OpenApiResponse):
            serializer = serializer.response

        return super()._is_list_view(serializer)

    def _get_paginator(self):
        """Disable pagination if the view's serializer specifically
        turned it off"""
        # Extract serializer instance
        serializer = self.get_response_serializers()
        if isinstance(serializer, dict) and serializer:
            # extract likely main serializer from @extend_schema override
            serializer = {str(code): s for code, s in serializer.items()}
            serializer = serializer[min(serializer)]

        # Get the corresponding serializer from OpenApiResponse
        if isinstance(serializer, OpenApiResponse):
            serializer = serializer.response

        serializer = force_instance(serializer)

        # Check if pagination is disabled
        try:
            if not serializer.child.Meta.paginated:
                return None
        except AttributeError:
            pass

        # Check if output is of batch action views
        try:
            if self.view.action.startswith('batch_'):
                return None
        except AttributeError:
            pass

        return super(AutoSchema, self)._get_paginator()

    def _is_create_operation(self):
        """Makes batch create a create operation"""
        if getattr(self.view, 'action', None) == 'batch_create':
            return True

        return super(AutoSchema, self)._is_create_operation()
