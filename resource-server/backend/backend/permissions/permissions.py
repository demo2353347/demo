import logging

from backend.client import KeycloakClient
from backend.permissions.mappings import VIEW_RESOURCE_MAPPING, VIEW_IAM_ACTION_MAPPING
from django.conf import settings
from keycloak.exceptions import KeycloakPostError
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.permissions import BasePermission

logger = logging.getLogger(__name__)


class IsAuthenticated(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated)


class HasUserPermission(BasePermission):
    message = 'You do not have permission to perform action on given resource'

    def __init__(self):
        self.kc_client = KeycloakClient()

    def has_permission(self, request, view):
        try:
            _, token = request.headers['Authorization'].split()
        except KeyError:
            self.message = 'Authorization header missing from request'
            return False

        try:
            token_info = self.kc_client.load_token(token)
        except AuthenticationFailed as e:
            self.message = str(e)
            return False

        if not settings.ENABLE_RPT:
            request.rpt = token
            return True

        if token_info.get('authorization'):
            rpt = token
            rpt_info = token_info

        else:
            try:
                rpt = self.kc_client.obtain_rpt(access_token=token)['access_token']
                rpt_info = self.kc_client.load_token(rpt)
            except KeycloakPostError:
                self.message = "You don't have permission to perform action on given resource"
                return False

        view_fq_name = f'{view.__class__.__module__}.{view.__class__.__qualname__}'
        resource_urn = VIEW_RESOURCE_MAPPING.get(view_fq_name)
        if not resource_urn:
            logger.warning('View {view_fq_name} has not been declared in VIEW_RESOURCE_MAPPING')
        else:
            for permission in rpt_info['authorization']['permissions']:
                if (permission['rsname'] == resource_urn
                        and VIEW_IAM_ACTION_MAPPING[view.action] in permission['scopes']):
                    request.rpt = rpt
                    return True

        self.message = 'You do not have permission to perform action on given resource ' + resource_urn
        return False
