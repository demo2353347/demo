VIEW_IAM_ACTION_MAPPING = {
    "create": "create",
    "batch_create": "create",
    "update": "update",
    "partial_update": "update",
    "destroy": "delete",
    "list": "list",
    "retrieve": "get"
}
VIEW_RESOURCE_MAPPING = {
    "k8s.views.K8sClusterViewSet": "resource:k8s:cluster",
    "k8s.views.K8sControlNodeViewSet": "resource:k8s:control_node",
    "k8s.views.K8sWorkerNodeViewSet": "resource:k8s:worker_node",
    "server.views.ServerViewSet": "resource:server:server",
    "load_balancer.views.LoadBalancerViewSet": "resource:load_balancer:load_balancer",
    "load_balancer.views.LBServerViewSet": "resource:load_balancer:lb_server",
    "storage.views.VolumeViewSet": "resource:storage:volume",
    "certificate.views.CertificateViewSet": "resource:certificate:cert"
}
