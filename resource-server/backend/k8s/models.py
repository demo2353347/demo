from backend.models import BaseModel
from django.db import models

from k8s.choices import CNI_TYPE_CHOICES, CSI_TYPE_CHOICES, DISK_TYPE_CHOICES, DISK_TYPE_SSD
from server.models import Server
from storage.models import Volume
from tenant.models import User


class CNIVersion(BaseModel):
    name = models.CharField(blank=True, unique=True)

    type = models.CharField(
        max_length=100,
        choices=CNI_TYPE_CHOICES,
    )
    version = models.CharField(
        max_length=100,
    )

    class Meta:
        ordering = ('type', 'version')
        unique_together = ('type', 'version')

    def __str__(self):
        return f'{self.type} - {self.version}'

    def save(self, **kwargs):
        if not self.name:
            self.name = str(self)
        super().save(**kwargs)


class CSIVersion(BaseModel):
    name = models.CharField(
        blank=True,
        unique=True)
    type = models.CharField(
        max_length=100,
        choices=CSI_TYPE_CHOICES,
    )
    version = models.CharField(
        max_length=100,
    )

    class Meta:
        ordering = ('type', 'version')
        unique_together = ('type', 'version')

    def __str__(self):
        return f'{self.type} - {self.version}'

    def save(self, **kwargs):
        if not self.name:
            self.name = str(self)
        super().save(**kwargs)


class K8sCluster(BaseModel):
    name = models.CharField(
        max_length=100,
        unique=True,
    )
    etcd_version = models.CharField()
    k8s_version = models.CharField()
    master_count = models.PositiveIntegerField(
        blank=True,
        default=1
    )
    worker_count = models.PositiveIntegerField(
        blank=True,
        default=1
    )
    cni_version = models.ForeignKey(
        to=CNIVersion,
        on_delete=models.PROTECT,
    )
    csi_version = models.ForeignKey(
        to=CSIVersion,
        on_delete=models.PROTECT,
    )


class K8sControlNode(BaseModel):
    cluster = models.ForeignKey(
        to=K8sCluster,
        on_delete=models.CASCADE,
        related_name='master_nodes'
    )
    ram = models.PositiveIntegerField(
        default=0
    )
    cpu = models.PositiveIntegerField(
        default=0
    )
    disk_type = models.CharField(
        max_length=20,
        choices=DISK_TYPE_CHOICES,
        default=DISK_TYPE_SSD
    )
    disk_size = models.IntegerField(
        null=True,
        blank=True,
    )
    server = models.ForeignKey(
        to=Server,
        null=True,
        on_delete=models.PROTECT,
        related_name='master_node'
    )
    volume = models.ForeignKey(
        to=Volume,
        null=True,
        on_delete=models.PROTECT,
        related_name='master_node'
    )


class K8sWorkerNode(BaseModel):
    cluster = models.ForeignKey(
        to=K8sCluster,
        on_delete=models.CASCADE,
        related_name='worker_nodes'
    )
    ram = models.PositiveIntegerField(
        default=0
    )
    cpu = models.PositiveIntegerField(
        default=0
    )
    disk_type = models.CharField(
        max_length=20,
        choices=DISK_TYPE_CHOICES,
        default=DISK_TYPE_SSD
    )
    disk_size = models.IntegerField(
        null=True,
        blank=True,
    )
    server = models.ForeignKey(
        to=Server,
        null=True,
        on_delete=models.PROTECT,
        related_name='worker_node'
    )
    volume = models.ForeignKey(
        to=Volume,
        null=True,
        on_delete=models.PROTECT,
        related_name='worker_node'
    )
