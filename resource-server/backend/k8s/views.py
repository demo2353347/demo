import requests
from backend.permissions.permissions import IsAuthenticated
from backend.views import ModelViewSet, ReadOnlyModelViewSet
from django.conf import settings
from django.db import transaction
from rest_framework.exceptions import PermissionDenied, ValidationError, APIException

from k8s.models import K8sCluster, K8sControlNode, K8sWorkerNode, CNIVersion, CSIVersion
from k8s.serializers import K8sClusterSerializer, K8sControlNodeSerializer, K8sWorkerNodeSerializer, \
    CNIVersionSerializer, CSIVersionSerializer


class CNIVersionViewSet(ReadOnlyModelViewSet):
    queryset = CNIVersion.objects.all()
    serializer_class = CNIVersionSerializer
    permission_classes = [IsAuthenticated]


class CSIVersionViewSet(ReadOnlyModelViewSet):
    queryset = CSIVersion.objects.all()
    serializer_class = CSIVersionSerializer
    permission_classes = [IsAuthenticated]


class K8sClusterViewSet(ModelViewSet):
    queryset = K8sCluster.objects.all()
    serializer_class = K8sClusterSerializer

    def create_resource(self, path, **data):
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer " + self.request.rpt
        }
        response = requests.post(f'{settings.API_URL}/{path}', data=data, headers=headers)
        if response.ok:
            return response.json()
        elif response.status_code == 403:
            raise PermissionDenied(response.json())
        elif response.status_code == 400:
            raise ValidationError(response.json())
        else:
            raise APIException(response.content)

    def perform_create(self, serializer):
        with transaction.atomic():
            cluster = serializer.save()

            self.create_resource(
                'load_balancer/load-balancers/',
                name=f'{cluster.name}_lb',
                public_port=8443,
                description=f'K8s Cluster {cluster.name} Load Balancer'
            )

            for i in range(cluster.master_count):
                node = K8sControlNode.objects.create(cluster=cluster)
                volume = self.create_resource(
                    '/storage/volumes/',
                    name=f'{cluster.name}_master_volume_{i}',
                    size=80,
                    type="ssd",
                )
                server = self.create_resource(
                    'server/servers/',
                    name=f'{cluster.name}_master_server_{i}',
                    os="Ubuntu 20.04",
                    ram=8,
                    cpu=4,
                    ip=f"10.0.0.{i + 1}",
                    disk_size=80,
                )
                node.server_id = server['id']
                node.volume_id = volume['id']
                node.save()

            for i in range(cluster.worker_count):
                node = K8sWorkerNode.objects.create(cluster=cluster)
                volume = self.create_resource(
                    '/storage/volumes/',
                    name=f'{cluster.name}_worker_volume_{i}',
                    size=80,
                    type="ssd",
                )
                server = self.create_resource(
                    'server/servers/',
                    name=f'{cluster.name}_worker_server_{i}',
                    os="Ubuntu 20.04",
                    ram=8,
                    cpu=4,
                    ip=f"10.0.0.{i + 1}",
                    disk_size=80,
                )
                node.server_id = server['id']
                node.volume_id = volume['id']
                node.save()


class K8sControlNodeViewSet(ModelViewSet):
    queryset = K8sControlNode.objects.all()
    serializer_class = K8sControlNodeSerializer


class K8sWorkerNodeViewSet(ModelViewSet):
    queryset = K8sWorkerNode.objects.all()
    serializer_class = K8sWorkerNodeSerializer
