from django.contrib import admin

from k8s.models import K8sCluster, CNIVersion, CSIVersion, K8sControlNode, K8sWorkerNode


@admin.register(CNIVersion)
class CNIVersionAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'version')


@admin.register(CSIVersion)
class CSIVersionAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'version')


# Register your models here.
@admin.register(K8sCluster)
class K8sClusterAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'k8s_version', 'etcd_version', 'master_count', 'worker_count',
                    'cni_version', 'csi_version')


@admin.register(K8sControlNode)
class K8sControlNodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'cluster', 'ram', 'cpu', 'disk_type', 'disk_size')


@admin.register(K8sWorkerNode)
class K8sWorkerNodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'cluster', 'ram', 'cpu', 'disk_type', 'disk_size')
