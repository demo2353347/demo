from rest_framework import routers

from k8s.views import CNIVersionViewSet, CSIVersionViewSet, K8sClusterViewSet, K8sControlNodeViewSet, \
    K8sWorkerNodeViewSet

router = routers.DefaultRouter()
router.register('cni-versions', CNIVersionViewSet)
router.register('csi-versions', CSIVersionViewSet)
router.register('clusters', K8sClusterViewSet)
router.register('control-nodes', K8sControlNodeViewSet)
router.register('worker-nodes', K8sWorkerNodeViewSet)

urlpatterns = router.urls
