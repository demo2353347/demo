from backend.serializers import BaseModelSerializer
from rest_framework import serializers

from k8s.models import K8sCluster, K8sControlNode, K8sWorkerNode, CSIVersion, CNIVersion


class CNIVersionSerializer(BaseModelSerializer):
    class Meta:
        model = CNIVersion
        fields = '__all__'


class CSIVersionSerializer(BaseModelSerializer):
    class Meta:
        model = CSIVersion
        fields = '__all__'


class K8sClusterSerializer(BaseModelSerializer):
    master_count = serializers.IntegerField(default=2)
    worker_count = serializers.IntegerField(default=2)
    cni_version = serializers.SlugRelatedField(queryset=CNIVersion.objects.all(), slug_field='name')
    csi_version = serializers.SlugRelatedField(queryset=CSIVersion.objects.all(), slug_field='name')

    class Meta:
        model = K8sCluster
        fields = ['id', 'name', 'k8s_version', 'etcd_version', 'master_count', 'worker_count',
                  'cni_version', 'csi_version']


class K8sControlNodeSerializer(BaseModelSerializer):
    class Meta:
        model = K8sControlNode
        fields = ['id', 'cluster', 'ram', 'cpu', 'disk_type', 'disk_size']


class K8sWorkerNodeSerializer(BaseModelSerializer):
    class Meta:
        model = K8sWorkerNode
        fields = ['id', 'cluster', 'ram', 'cpu', 'disk_type', 'disk_size']
