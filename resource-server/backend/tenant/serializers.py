from backend.serializers import BaseModelSerializer

from tenant.models import User


class UserSerializer(BaseModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name']
