from backend.models import BaseModel
from django.contrib.auth.models import AbstractUser, UserManager as _UserManager
from django.db import IntegrityError, router
from django.db import transaction


class UserManager(_UserManager):

    def get_or_create_user(self, user_info, lookup_key='pk'):
        """Implements double-check locking for user creation logic

        Inspired by the get_or_create method on the default model manager
        """
        params = {lookup_key: user_info[lookup_key]}
        try:
            return self.get(**params), False
        except self.model.DoesNotExist:
            db = self._db or router.db_for_write(self.model, **self._hints)
            try:
                with transaction.atomic(using=db):
                    return self.create_user(**user_info), True
            except IntegrityError:
                try:
                    return self.using(db).get(**params), False
                except self.model.DoesNotExist:
                    pass
                raise


class User(BaseModel, AbstractUser):
    objects = UserManager()

    def __str__(self):
        return self.username

    class Meta:
        ordering = ('username', 'pk')
