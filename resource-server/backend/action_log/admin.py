from django.contrib import admin

from action_log.models import ActionLog


# Register your models here.
@admin.register(ActionLog)
class ActionLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'path', 'method', 'username', 'remote_ip', 'status_code', 'datetime')
