from backend.serializers import BaseModelSerializer

from action_log.models import ActionLog


class ActionLogSerializer(BaseModelSerializer):
    class Meta:
        model = ActionLog
        fields = '__all__'
