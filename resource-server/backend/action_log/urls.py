from rest_framework import routers

from action_log.views import ActionLogViewSet
from k8s.views import CNIVersionViewSet, CSIVersionViewSet, K8sClusterViewSet, K8sControlNodeViewSet, \
    K8sWorkerNodeViewSet

router = routers.DefaultRouter()
router.register('action-logs', ActionLogViewSet)

urlpatterns = router.urls
