import uuid

from django.db import models


# Create your models here.

class ActionLog(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    path = models.CharField(
        null=False,
        max_length=254
    )
    method = models.CharField(
        max_length=20,
        null=False
    )
    username = models.CharField(
        blank=True
    )
    remote_ip = models.CharField(
        max_length=50,
        null=True
    )
    status_code = models.PositiveIntegerField(
        null=True,
        blank=True
    )
    datetime = models.DateTimeField(
        auto_now_add=True
    )

    class Meta:
        ordering = ['-datetime']
