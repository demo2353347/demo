from backend.views import ReadOnlyModelViewSet

from action_log.models import ActionLog
from action_log.serializers import ActionLogSerializer


# Create your views here.
class ActionLogViewSet(ReadOnlyModelViewSet):
    queryset = ActionLog.objects.all()
    serializer_class = ActionLogSerializer