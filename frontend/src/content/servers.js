export default [
  {
    'id': 1,
    'name': 'cluster-1-master-1',
    'os': 'Ubuntu 22.04',
    'cpus': 4,
    'ram': 8,
    'ip_address': '192.168.1.1',
    'root_disk_size': 50,
    'status': 'active'
  }
]