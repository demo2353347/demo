export default [
  {
    text: '',
    items: [
      { icon: 'mdi-view-dashboard-outline', key: 'menu.dashboard', text: 'Dashboard', link: '/#' }
    ]
  }, {
    text: 'Platform',
    key: 'menu.platform',
    items: [
      { icon: 'mdi-file-outline', key: 'menu.k8s', text: 'K8s Clusters', link: '/kubernetes' },
    ]
  }, {
    text: 'Infrastructure',
    key: 'menu.infrastructure',
    items: [
      { icon: 'mdi-file-outline', key: 'menu.server', text: 'Servers', link: '/servers' },
      { icon: 'mdi-file-outline', key: 'menu.volume', text: 'Volumes', link: '/volumes' },
      { icon: 'mdi-file-outline', key: 'menu.load_balancer', text: 'Load Balancers', link: '/load_balancers' },
    ]
  }, {
    text: 'Secret',
    key: 'menu.secret',
    items: [
      { icon: 'mdi-file-outline', key: 'menu.certificate', text: 'Certificates', link: '/certificates' },
    ]
  }
]
