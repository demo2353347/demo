export default [
  {
    'id': 1,
    'name': 'K8s Cluster',
    'k8s_version': 'v1.28.0',
    'master_count': 3,
    'worker_count': 3,
    'status': 'updating',
    'api_server_url': 'https://my-cluster.k8s.io'
  }
]