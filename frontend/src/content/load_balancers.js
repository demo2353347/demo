export default [
  {
    'id': 1,
    'name': 'cluster-1-lb-1',
    'virtual_address': '192.168.1.1:80',
    'domain_name': 'example.com',
    'tls_configured': false,
    'member_addresses': ['192.168.1.1', '192.168.1.2', '192.168.1.3'],
    'status': 'active'
  }
]