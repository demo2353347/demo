### Carbon UI Kit
| https://indielayer.com

- Vue UI Kit
- Vuetify - Number #1 UI Vue Component Library

### Documentation

See - `CarbonUI-Documentation.pdf`

## Commands
#### `yarn`
> Installs package dependencies

#### `yarn dev`
> Compiles and hot-reloads for development

#### `yarn build`
> Compiles and minifies for production
